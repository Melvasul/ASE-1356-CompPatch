version="1"
tags={
	"Expansion"
}
dependencies={
	"Ages and Splendor Expanded"
	"1356 - A Timeline Extension Mod for EUIV"
}
name="CompPatch for ASE and 1356"
supported_version="1.33.3"
remote_file_id="2593020190"